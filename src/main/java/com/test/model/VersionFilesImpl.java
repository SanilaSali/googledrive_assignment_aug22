package com.test.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.test.model.VersionFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class VersionFilesImpl {
	
private JdbcTemplate jdbcTemplate;
	
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
public boolean saveVersionHistory(VersionFiles versionFiles) throws SQLException{
    	
    	boolean status = false;
    	boolean documentExists = false;
    	int rowsInserted = 0;
    	int document_id = 0;
    	String docname = versionFiles.getDocument();
    	String version_name = versionFiles.getVersion();
   		Timestamp created_on = new java.sql.Timestamp(versionFiles.getCreatedDate().getTime());
   		long created_date = created_on.getTime();
   		String created_by = "admin";
   		
   		System.out.println("IN saveVersionHistory docname, created_date " + docname + created_date);
    	try{ 
          	    //check if document exists - START
    	    	String SQL_DOCUMENT_SELECT = "select document_id from document where name = '"+docname+"'" ;
    	    	List<VersionFiles> savedFiles = this.jdbcTemplate.query(SQL_DOCUMENT_SELECT, new BeanPropertyRowMapper(VersionFiles.class));
    	    	System.out.println("IN saveVersionHistory savedFiles.size() "+savedFiles.size());
    			if (savedFiles.size()>0){
    				documentExists = true;
    				VersionFiles savedDocument = savedFiles.get(0);
    				
    			}
			    else 
			    	documentExists = false;
    			System.out.println("IN saveVersionHistory documentExists "+documentExists);
    			if(documentExists){
    				
    				status = true;
    			}else{
    				System.out.println("IN DOCUMENT DOES NOT EXISTS ");
    				String SQL_DOCUMENT_INSERT = "INSERT INTO document (name, created_on, created_by) VALUES (?, ?, ?)";
    	    		String SQL_VERSION_INSERT = "INSERT INTO version (version_name, document_id, created_on, created_by) VALUES (?, ?, ?, ?)";
    	    		
    	    		Object[] params = { docname, created_on, created_by};
        	   		int[] types = {Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR};
        	   		int rows = this.jdbcTemplate.update(SQL_DOCUMENT_INSERT, params, types);
        	   		System.out.println("IN saveVersionHistory DOCUMENT DOES NOT EXISTS - rows "+rows);
        	   		
        	   		String SQL_DOCUMENT_INSERTED = "select document_id from document where name = '"+docname+"'" ;
        	    	List<VersionFiles> insertedFiles = this.jdbcTemplate.query(SQL_DOCUMENT_INSERTED, new BeanPropertyRowMapper(VersionFiles.class));
        	    	System.out.println("IN saveVersionHistory DOCUMENT DOES NOT EXISTS - insertedFiles.size() "+insertedFiles.size());
        			if (insertedFiles.size()>0){
        				VersionFiles insertedDocument = insertedFiles.get(0);
        				document_id = insertedDocument.getDocument_id();
        				Object[] versionParams = { version_name, document_id, created_on, created_by};
            	   		int[] versionTypes = {Types.VARCHAR, Types.INTEGER, Types.TIMESTAMP, Types.VARCHAR};
            	   		int versionRows = this.jdbcTemplate.update(SQL_VERSION_INSERT, versionParams, versionTypes);
            	   		System.out.println("IN saveVersionHistory DOCUMENT DOES NOT EXISTS - versionRows "+versionRows);
        			}
        			status = true;
    			}
    	}catch(Exception e){}
    	return status;
	}

public List<VersionFiles> getVersionHistory(){
	List<VersionFiles> versionHistory = new ArrayList<VersionFiles>();
	System.out.println("IN getVersionHistory");
	try{
		
		String SQL_GET_DOCUMENT_VERSIONS = "select d.name, v.version_name, v.created_on, v.created_by from "
					+"document d RIGHT JOIN version v on d.document_id =v.document_id order by v.version_name asc";
		//versionHistory = this.jdbcTemplate.query(SQL_GET_DOCUMENT_VERSIONS, new BeanPropertyRowMapper(VersionFiles.class));
		versionHistory = this.jdbcTemplate.query(SQL_GET_DOCUMENT_VERSIONS,new RowMapper<VersionFiles>() {
		     public VersionFiles mapRow(ResultSet rs, int rowNum)
		    	       throws SQLException {
		    	 VersionFiles versionFiles = new VersionFiles();
		    	 versionFiles.setDocument(rs.getString("name"));
		    	 versionFiles.setVersion(rs.getString("version_name"));
		    	 versionFiles.setCreatedDate(rs.getDate("created_on"));
		    	 versionFiles.setCreatedBy(rs.getString("created_by"));
		     return versionFiles;
		    	     }
		    	    });
		
		System.out.println("IN getVersionHistory - insertedFiles.size() "+versionHistory.size());
		for (VersionFiles versionFiles : versionHistory){
			System.out.println("*********************************************************");
			System.out.println("versionFiles.getDocument():" +versionFiles.getDocument());
			System.out.println("versionFiles.getVersion():" +versionFiles.getVersion());
		}
	}catch(Exception e){}
		
	return versionHistory;
}

}
